# -*- encoding: utf-8 -*-
require File.expand_path('../lib/omniauth-enlighten/version', __FILE__)

Gem::Specification.new do |gem|
  gem.name          = 'omniauth-enlighten'
  gem.version       = OmniAuth::Enlighten::VERSION
  gem.author        = 'Sivakumar Radhakrishnan'
  gem.email         = 'sradhakrishnan@enphaseenergy.com'
  # gem.license       = 'MIT'
  gem.homepage      = 'https://bitbucket.org/enphaseembedded/sm-omniauth-enlighten/src'
  gem.summary       = 'OmniAuth strategy for Enlighten OAuth1.0 XAuth'
  gem.description   = 'OmniAuth strategy for Enlighten OAuth1.0 XAuth'

  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.require_paths = ['lib']

  # Production Dependencies
  gem.add_dependency 'omniauth', '2.1.2'
  gem.add_runtime_dependency 'oauth', '1.1.0'
  gem.add_runtime_dependency 'omniauth-oauth', '1.2.0'
  gem.add_runtime_dependency 'multi_json', '1.15.0'
  # Development Dependencies
  gem.add_development_dependency 'rake', '13.1.0'
  gem.add_development_dependency 'rdiscount', '~> 2.2.7'
  gem.add_development_dependency 'yard', '0.9.34'
  gem.add_development_dependency 'rspec', '3.12.0'
  gem.add_development_dependency 'webmock', '3.19.1'
  gem.add_development_dependency 'simplecov', '0.22.0'
  gem.add_development_dependency 'rack-test', '2.1.0'
end
