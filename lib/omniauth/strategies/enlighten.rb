require 'omniauth'
require 'multi_json'
require 'oauth'
# Author: Jaison Brooks, 2017

module OmniAuth
  module Strategies
    class Enlighten
      include OmniAuth::Strategy
      attr_reader :access_token
      args [:consumer_key, :consumer_secret]
      option :consumer_key, nil
      option :consumer_secret, nil
      option :name, 'enlighten'
      option :client_options, {
        :signature_method   => 'HMAC-SHA1',
        :access_token_url => "https://api#{Rails.env.production? ? '' : '-qa2'}.enphaseenergy.com/oauth/access_token",
        :site => "https://api#{Rails.env.production? ? '' : '-qa2'}.enphaseenergy.com",
        :oauth_version      => '1.0',
        :request_token_path => nil,
        :authorize_path     => nil,
      }

      # user_data Hash
      info do
        {
          'active'          => raw_info['active'],
          'user_roles'      => raw_info['user_roles'],
          'email'           => raw_info['email'],
          'enlighten_emails'=> raw_info['enlighten_emails'],
          'enlighten_view'  => raw_info['enlighten_view'],
          'first_name'      => raw_info['first_name'],
          'last_name'       => raw_info['last_name'],
          'phone'           => raw_info['phone'],
          'updated_at'      => raw_info['updated_at'],
          'uri'             => raw_info['uri'],
          'user_id'         => raw_info['user_id'],
          'company'         => {
            'uri'           => raw_info['company']['uri'],
            'company_id'    => raw_info['company']['company_id'],
            'company_name'  => raw_info['company']['company_name'],
            'location'      => raw_info['company']['location'],
            'roles'         => raw_info['company']['roles']
          }
        }
      end

      # Setup raw info Hash
      extra do
        {
          'raw_info' => raw_info
        }
      end

      # Setup UID
      uid{ raw_info['user_id'] }

      # Prepare OAuth1.0 xAuth request
      def request_phase
        puts "Omniauth 1.0: Entered Request Phase"
        session['omniauth.auth'] = { 'x_auth_mode' => 'client_auth', 'x_auth_username' => request.params['username'], 'x_auth_password' => request.params['password'] }
        redirect callback_path
      end

      def consumer
        consumer = ::OAuth::Consumer.new(options.consumer_key, options.consumer_secret, options.client_options)
        consumer.http.open_timeout = options.open_timeout if options.open_timeout
        consumer.http.read_timeout = options.read_timeout if options.read_timeout
        consumer
      end

      # Get the User Hash
      def raw_info
        @raw_info ||= MultiJson.decode(access_token.get('/api/v2/users/self?expand=company').body)
      end


      # Callback from OAuth request
      def callback_phase
        puts "Omniauth 1.0: Entered Callback Phase"
        raise OmniAuth::NoSessionError.new('Session Expired') if session['omniauth.auth'].nil?
        @access_token = consumer.get_access_token(nil, {}, session['omniauth.auth'])
        super
        # Save the Errors!
      rescue ::Net::HTTPFatalError, ::OpenSSL::SSL::SSLError => e
        fail!(:service_unavailable, e)
      rescue ::OAuth::Unauthorized => e
        fail!(:invalid_credentials, e)
      rescue ::MultiJson::DecodeError => e
        fail!(:invalid_response, e)
      rescue ::OmniAuth::NoSessionError => e
        fail!(:session_expired, e)
      rescue => e
        puts "Omniauth 1.0: Error during processing: #{$!}"
        puts "Omniauth 1.0: Backtrace:\n\t#{e.backtrace.join("\n\t")}"
      ensure
        session['omniauth.auth'] = nil
      end

      # Store token credientials
      credentials do
        { 'token' => @access_token.token, 'secret' => @access_token.secret }
      end

    end
  end
end

OmniAuth.config.add_camelization 'enlighten', 'Enlighten'
