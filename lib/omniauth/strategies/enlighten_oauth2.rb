require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class EnlightenOAuth2 < OmniAuth::Strategies::OAuth2
      args [:client_id, :client_secret]
      option :client_id, nil
      option :client_secret, nil
      option :name, 'enlighten'

      attr_accessor :access_token

      # user_data Hash
      info do
        {
          'active'          => raw_info['active'],
          'user_roles'      => raw_info['user_roles'],
          'email'           => raw_info['email'],
          'enlighten_emails'=> raw_info['enlighten_emails'],
          'enlighten_view'  => raw_info['enlighten_view'],
          'first_name'      => raw_info['first_name'],
          'last_name'       => raw_info['last_name'],
          'phone'           => raw_info['phone'],
          'updated_at'      => raw_info['updated_at'],
          'uri'             => raw_info['uri'],
          'user_id'         => raw_info['user_id'],
          'company'         => {
            'uri'           => raw_info['company']['uri'],
            'company_id'    => raw_info['company']['company_id'],
            'company_name'  => raw_info['company']['company_name'],
            'location'      => raw_info['company']['location'],
            'roles'         => raw_info['company']['roles']
          }
        }
      end

      # Setup raw info Hash
      extra do
        {
        'raw_info' => raw_info
        }
      end

      # Setup UID
      uid { raw_info['user_id'] }

      def initialize(app, *args, &block)
        super
        options.client_options.token_url = ENV['ENLIGHTEN_API_AUTH2_TOKEN_URL']
        options.client_options.site = ENV['ENLIGHTEN_API_AUTH2_SITE']
      end

      # 4. Getting user information
      def raw_info
        @raw_info ||= JSON.parse(access_token.get('/api/v4/users/self?expand=company').body)
      end

      # 1. Overriding Request Phase of OAuth2
      def request_phase
        puts "Omniauth 2.0: Entered Request Phase"
        session['omniauth.auth'] = {
          'grant_type' => 'password',
          'username' => request.params['username'],
          'password' => request.params['password']
        }
        redirect callback_path
      end

      # 2. Overriding Callback Phase of OAuth2
      def callback_phase
        begin
          puts "Omniauth 2.0: Entered Callback Phase"
          raise OmniAuth::NoSessionError.new('Session Expired') if session['omniauth.auth'].nil?
          self.access_token = build_access_token
          OmniAuth::Strategy.instance_method(:callback_phase).bind(self).call
          # Save the Errors!
        rescue ::OAuth2::Error, CallbackError => e
          fail!(:invalid_credentials, e)
        rescue ::Timeout::Error, ::Errno::ETIMEDOUT => e
          fail!(:timeout, e)
        rescue ::SocketError => e
          fail!(:failed_to_connect, e)
        rescue => e
          puts "Omniauth 2.0: Error during processing: #{$!}"
          puts "Omniauth 2.0: Backtrace:\n\t#{e.backtrace.join("\n\t")}"
        ensure
          session['omniauth.auth'] = nil
        end
      end

      protected

      # 3. Getting the Access token for the client i.e., Service Manager
      def build_access_token
        puts "Omniauth 2.0: Building access token"
        token_request_params = {
          headers: {
            "Content-Type" => "application/x-www-form-urlencoded",
            "Authorization" => "Basic #{Base64.strict_encode64("#{options.client_id}:#{options.client_secret}")}"
          },
          body: session['omniauth.auth']
        }
        response = client.request(:post, client.token_url, token_request_params)
        ::OAuth2::AccessToken.from_hash(client, JSON.parse(response.response.body))
      end
    end
  end
end

OmniAuth.config.add_camelization 'enlighten_oauth2', 'EnlightenOAuth2'